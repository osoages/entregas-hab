/**
 * Entregable semana 2
 * 
 * Escribe el código necesario para decidir en qué
 * fotografías sale Pablo. Como resultado se debe
 * obtener un array de strings con los nombres de las
 * fotografías.
 *  
 */

 const photos = [
    {
        name: 'Cumpleaños de 13',
        people: ['Maria', 'Pablo']  
    },
    {
        name: 'Fiesta en la playa',
        people: ['Pablo', 'Marcos']  
    },    
    {
        name: 'Graduación',
        people: ['Maria', 'Lorenzo']  
    },
 ]

 const peopleToFind = 'Pablo';
 
 let salida = [];
 
 for (let photo of photos){
        for (let i=0; i< photo.people.length; i++){
            if(photo.people[i] == peopleToFind){
                salida.push(photo.name)
            }
        }
 } 

 console.log (`El string generado con las fotos de ${peopleToFind} es: \n${salida}`);
