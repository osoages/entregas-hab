/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 */

const loanJs = require('loanjs')

const argv = process.argv; //Almaceno TODA la entrada por consola en la varibale arg
//Asigno variables a raíz del array argv (A mayores aplico un parseInt para asegurarme de que sean ńumeros)
const capital = parseInt(argv[2]);
const plazos = parseInt(argv[3]);
const intereses = parseInt(argv[4]);

//Si el vlaor de argv es mayor a 5 aviso por pantalla de que śolo se tendrán 
//en cuenta los primeros 3 datos introducidos por la línea de comandos
if(argv.length > 5){
    console.log(`Se han introducido datos erróneos, debe introducidor los siguientes datos por orden: "capital", "plazos", "tipo de interes"`);
    console.log(`Sobran los datos introducidos desde: "${argv[5]}" en adelante, sólo se tendrán en cuenta para el cálculo los siguiente datos:`);
    console.log(`|| Capital: ${capital}€|| Plazos: ${plazos} meses || Tipo de interés: ${intereses}%`)

} else if(argv.length <= 4){
    console.log(`No se han introducido datos válidos para el cálculo, revise los datos introducidos:\n|| Capital: ${capital}€|| Plazos: ${plazos} meses || Tipo de interés: ${intereses}%`)
    
    console.log()
} else {
    loan = loanJs.Loan(capital, plazos, intereses)
    console.log(loan.length)
    for (i=0; i < loan.length; i++){
        console.log(loan[0])
    }
    //console.log(loan{installment});
}
