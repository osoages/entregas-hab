// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/posts/1

// a) Generar contador de mensajes por usario
// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/  
const axios = require('axios');
const urlApi = 'https://jsonplaceholder.typicode.com/posts';
//Primera parte del ejercicio contador de mensajes realizados por cada usuario
async function contadorMensajesUsuario(){
    
    const responseJsonData1 = await axios.get(urlApi);
    const JsonData1ResponseOk = responseJsonData1.data;

    let arrayResponse1 = {};//Genero un objeto de salida para la función donde almacenaré el nº de id de cada usuario y su numero de mensajes.
    //Itero sobre el array para acceder a cada elemento de este (Me interesa el userId)
    
    for ( let dataJson of JsonData1ResponseOk){
       const dataJsonuserId = dataJson.userId;//Asigno la variable "dataJsonuserId" elemento que me interesa del objeto
       
       if (arrayResponse1[dataJsonuserId] === undefined){            
            arrayResponse1[dataJsonuserId] = 1;
       }
       else {
           arrayResponse1[dataJsonuserId]++
       }
    }
    return console.log(arrayResponse1);
}


async function generarListaTitleBody(){
    const responseInitial = await axios.get(urlApi);
    const responseData = responseInitial.data;

    detailedPosts = {};
    
    for (response of responseData){
        response = await axios.get(`${urlApi}/${response.id}`)
        detailedResponse = response.data;
        
        //const {userId, id, title, body} = detailedResponse;
       
        //comprobar si en usrs ya existe la entrada para el usrd
        if (detailedPosts[detailedResponse.userId] === undefined){
            detailedPosts[detailedResponse.userId] = {
                userId: detailedResponse.userId,
                posts: [
                    {
                        title: detailedResponse.title,
                        body: detailedResponse.body
                    }
                ]
            }
        }else {
            detailedPosts[detailedResponse.userId].posts.push(
                {
                title: detailedResponse.title, 
                body: detailedResponse.body
                }
            );
        }
        //console.log(detailedPosts)
    }
    
    /* Comprobación del contenido del objeto con el título y el body.
    for (key of Object.keys (detailedPosts)){
        console.log(detailedPosts[key].posts)
    }
    */

    console.log(detailedPosts)//Para sacar por consola el resultado de la función cuado se llamada
}
//ejecuto la llamada a la función 2
generarListaTitleBody().then(()=>{
});

//contadorMensajes = contadorMensajesUsuario().then(()=>{});
//generarLista().then(()=>{});
