import config from './config.js';

const axios = require('axios').default;

const apiKey = config.apiKey; 
const BASE_URL = "https://ws.audioscrobbler.com/";
const URL_GEO = "2.0/?method=geo.gettopartists&country=spain&api_key="+apiKey+"&format=json";

const URL_TOPTRACKS = "2.0/?method=geo.gettoptracks&country=spain&api_key="+apiKey+"&format=json";
const URL_TOPTAGS = "2.0/?method=chart.gettoptags&api_key="+apiKey+"&format=json";


async function getArtists(){
    try{
        const response = await axios.get(`${BASE_URL}${URL_GEO}`);
        return response;        
    } catch(error){
        console.error(error);
    }
}

//Para comprobar la salida por consola de la respuesta de la api a la llamada de axios
/*
function getTopTracks(){
    axios.get(`${BASE_URL}${URL_TOPTRACKS}`)
        .then(function(response){
            console.log(response)
        })
        .catch(function(error){
            console.log(error)
        })
}
*/
async function getTopTracks(){
    try{
        const response = await axios.get(`${BASE_URL}${URL_TOPTRACKS}`);
        console.log(response)
        return response;
    } catch(error){
        console.error(error);
    }

}

//Para comprobar la salida por consola de la respuesta de la api a la llamada de axios
/*function getTopTags(){
    axios.get(`${BASE_URL}${URL_TOPTAGS}`)
        .then(function(response){
            console.log(response)
        })
}
*/
async function getTopTags(){
    try{
        const response = await axios.get(`${BASE_URL}${URL_TOPTAGS}`);
        return response;
    } catch (error){
        console.error(error);
    }
}

export default {
    getArtists,
    getTopTracks,
    getTopTags    
}