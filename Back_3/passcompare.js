const bcrypt = require('bcrypt');

(async () => {
   
    const passwordIsvalid = await bcrypt.compare(process.argv[2], '$2b$10$.tga5no7Sy9HqjGPct6hL.wcRzrD2P5m0LOjgTWHEZiC566Xr6n82');

    console.log(passwordIsvalid)
})()