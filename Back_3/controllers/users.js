const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//Database temporal de usuarios: 
const bd = require('./bd_mock')


/*
const { email, password } = req.body;

    //Validación de datos de login
    if(!email || !password){
        res.status(400).send();
        return;
    }

    const passwordBcrypt = bcrypt.hash(password, 10);

    res.send();*/

const login = (req,  res) => {

    //Recupero los datos del body de la petición POST
    const {email, password} = req.body;
    
    //Busco el mail en la bd. Devuelve un  JSOn con Id, rol y psswd
    const user = bd.getUser(email);
    
    //si no existe devovler un error
    if(!user){
        res.status(400).send();
        return;
    };

    //comprobar la password encriptada

    //error si no coinciden
    const passwordIsValid = bcrypt.compare(password, user.password)

    if(!passwordIsValid){
        res.status(401).send();
        return;
    }
    
    const tokenPayload = {id: users.id, role: users.role};
    //Firmo el token
    const token = jwt.sign(tokenPayload, process.env.SECRET,{
        expiresIn: '1d'
    });
    
    res.json({
        token
    });
}





/*const login2 = async (req, res) => {
    const { password } = req.body;

    const getEmail = bd.user.email;

    const tokenPayload = { 
        id: user.id, 
        role: user.role,
        email: user.email
    };

    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '1d'
    });

    res.json({
        token
    })
}
*/
module.exports = {
    login
}