//Usuario por defecto
const users = {
    id: 1,
    email: 'testing@testing',
    password: '$2b$10$.tga5no7Sy9HqjGPct6hL.wcRzrD2P5m0LOjgTWHEZiC566Xr6n82', //hashed passwd
    rol: 'admin'
}

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return users.find( matchEmail );
}



module.exports = {
    getUser
}