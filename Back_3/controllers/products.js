const bd = require('./bd_mock.js')

let id = 1;

let products = [];

const list = (req, res) => {
    res.json(products);
}

//Filtrado por queryString
const filteredList = (req, res) => {
    let filteredProducts = products;

    const filteredName = req.query['name'].toLowerCase();

    if(filteredName!== null || filteredName !== undefined) {

        filteredProducts = products.filter(product => product.name === filteredName)
   
        res.json(filteredProducts);
    }
    
}

const add = (req, res) => {
    //Validar si prize&stock -> es un valor numérico
    //TODO
    const inputName = req.body.name
    const inputStock = req.body.stock
    const inputPrize = req.body.prize
    
    const finder = products.find (product => product.name === inputName);

    //Validación de campo numérico
    if(isNaN(inputPrize)){ 
         res.status(409).send(`${inputPrize} no es un valor válido para el campo "Precio", introduzca un valor numérico`)
        return;
    } else if(isNaN(inputStock)){ 
        res.status(409).send(`${inputStock} no es un valor válido para el campo "Stock", introduzca un valor numérico`)
       return;
   }

    //Comprobar que los campos de entrada no están vacíos
    if (!inputName || !inputStock || !inputPrize){
       res.status(400).send();
       return;
    }
    //Comprobar si ya existe un producto con ese nombre
    if (finder){
        res.status(409).send(`Ya existe un producto dado de alta como:"${finder.name}"`);
        return;
    }
    
    products.push ({
        id: id++,
        name : inputName,
        stock : inputStock,
        prize : inputPrize
    });
    res.send();
}



const put = (req, res) => {
    //Pendiente
}

module.exports = {
    filteredList,
    list,
    add,
    put
}