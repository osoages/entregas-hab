Para poder realizar la comprobación y dado que el usuario se define de manera previa en la "bd", se debe de tomar en cuenta,
las siguientes consideraciones:

//Verificaciones hash
1º -  Se crea una password hasheada para el usuario con passGenerator.js (empleé 1234):
    $2b$10$.tga5no7Sy9HqjGPct6hL.wcRzrD2P5m0LOjgTWHEZiC566Xr6n82

2º- Se oyede enokear ek documento 'passCompare.js' pasando como argumento de entrada la password para comprobar si el hasheo es correcto
    node passCompare.js 1234 <- Si devuelve true es que es correcto.