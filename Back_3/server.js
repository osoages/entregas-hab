 require('dotenv').config();

 const bodyParser = require('body-parser');
 const cors = require('cors');
 const express = require('express');
 const morgan = require ('morgan');
 const port = process.env.PORT;

 //const { isAdmin } = require('./middlewares/auth.js')
 const {add, list, filteredList, put} = require ('./controllers/products');
 const {login} = require ('./controllers/users');

 const app = express();

 app.use(cors());
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({ extended: true }));

 app.use((req, res, next) => {
    console.log('código que se ejecuta antes de procesar el endpoint');
    next();
 })


 app.get('/products', list, filteredList);
 app.post('/products/login', filteredList )
 app.post('/products', add, isAuthenticated);



 app.listen(port, () =>{
    console.log(`Server up on PORT ${port}`)
 });