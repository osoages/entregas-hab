-- Se utiliza la tabla entregables Sql
-- Se da por sentado que un restaurante sólopuede ser gestionado por un único encargado, y que los encargados sólo pueden estar asignados a un restaurante
-- Se almacena un códigoReserva para identificar a todas las personas que acudan en una misma reserva al restaurante
USE entregablesql;
 
CREATE TABLE IF NOT EXISTS usuarios (
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(50) UNIQUE NOT NULL,
    contraseña VARCHAR(40) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
	nombre	VARCHAR(50) NOT NULL,
    apellidos VARCHAR (50) NOT NULL
);
CREATE TABLE IF NOT EXISTS restaurante (
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nombre	VARCHAR(50) NOT NULL,
    dirección VARCHAR(50) NOT NULL,
    aforo INT NOT NULL
);	
CREATE TABLE IF NOT EXISTS reserva (
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	codigoReserva INT NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    dni VARCHAR(9) NOT NULL,
    horaEntrada DATETIME NOT NULL,
    horaSalida DATETIME NOT NULL,
    id_usuario INT UNSIGNED,
	id_restaurante INT UNSIGNED,
    FOREIGN KEY (id_restaurante) REFERENCES restaurante(id),
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
);

CREATE TABLE IF NOT EXISTS encargado (
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(50) UNIQUE NOT NULL,
    contraseña VARCHAR(40) NOT NULL,
    nombre	VARCHAR(50) NOT NULL,
    apellidos VARCHAR (50) NOT NULL
);
