//Librerías instaladas
const bodyParser = require('body-parser');
const axios = require('axios');
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();

//Librerías empleados por la APP
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json ());

//Datos de conexión a la BBDD
const connection = mysql.createConnection({
    host:'localhost',
    user:'vue_user',
    password:'1234',
    database:'hackamarket'
}); 


//Conexión a la BBDD
connection.connect( error => {
    if(error) throw error
    console.log('Connection stablished')
})

//Puerto en el que se levanta la conexión a la bbdd
const PORT = 8090;

//Conexión a la BBDD
app.listen(PORT, () => console.log(`Api alive on ${PORT}`))

//Endpoint para recuperar todos los artículos
app.get('/products', (req,res) =>{

    //Secuencia SQL
    const sql = 'SELECT * FROM listaproductos';

    //Conexión a la BD
    connection.query(sql, (error, results) => {
        if (error) throw error;
        if (results.length > 0){
            res.json(results)
        } else {
            console.log('Sin productos')
        }
    })
});

//Endpoint para recuperar todos los clientes
app.get('/clients', (req,res) =>{

    //Secuencia SQL
    const sql = 'SELECT * FROM listaclientes';

    //Conexión a la BD
    connection.query(sql, (error, results) => {
        if (error) throw error;
        if (results.length > 0){
            res.json(results)
        } else {
            console.log('Sin clientes')
        }
    })
});

//Dar de alta un usuario
app.post('/users/add',(req,res) =>{
    const sql = 'INSERT INTO listaclientes SET ?';

    const newUser = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        password: req.body.password,
        email: req.body.email,
        imagen: req.body.imagen
    }

    //Conexión a la bd
    connection.query(sql, newUser, error =>{
        if (error) throw error;
        console.log('Usuario creado')
    })

});
//Endpoint para borrar un usuario
app.delete('/users/delete/:id',(req,res) =>{

    //ID del usuario
    const id = req.params.id;

    //Secuencia SQL
    const sql = `DELETE FROM listaclientes WHERE id='${id}'`;

    //Conexión a la BBDD
    connection.query(sql, error =>{
        if (error) throw error;
        console.log('Usuario eliminado')
    })
})

//Endpoint para editar usuarios
app.put('/users/update/:id',(req,res) =>{
    const id = req.params.id
    
    const nombre=req.body.nombre;
    const usuario = req.body.usuario;
    const password = req.body.password;
    const email = req.body.email;
    const imagen = req.body.imagen;

    //Secuencia SQL
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', password='${password}', email='${email}', imagen='${imagen}' WHERE id=${id}`;

    //Conexión a la BBDD
    connection.query(sql,error =>{
        if(error) throw error
        console.log(`Cliente modificado con éxito`)

    })
})