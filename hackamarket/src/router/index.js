import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Error from '../views/Error.vue'
import Products from '../views/Products.vue'
import Clients from '../views/Clients.vue'
import Register from '../views/Register.vue'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/products',
    name: 'Products',
    component: () => import('../views/Products.vue') 
  },
  {
    path: '/clients',
    name: 'Clients',
    component: () => import('../views/Clients.vue') 
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue') 
  },
  {
    path: '*',
    name: 'Error',
    component: Error
  }
]

const router = new VueRouter({
  routes
})

export default router
