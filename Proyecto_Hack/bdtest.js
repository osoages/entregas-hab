let users = [
    {
        userId: 1,
        firstName: 'Orlando',
        lastName: 'Soage',
        phone: '637666666',
        city: 'vigo',
        email: 'test@test',
        password: '$2b$12$S/G/uaUalBNsyEeQou76lOuU6W/D/GDIQZ1l1ScLcpUunchX3Ne6a',
        role: 'user'
      },
      {
        userId: 2,
        firstName: 'Pepe',
        lastName: 'Soage',
        phone: '637666666',
        city: 'vigo',
        email: 'testing@prueba',
        password: '$2b$12$S/G/uaUalBNsyEeQou76lOuU6W/D/GDIQZ1l1ScLcpUunchX3Ne6a',
        role: 'user'
      }
]

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return users.find(matchEmail);
}
const getUserById = (userId) => {
  
  const matchId = user => user.userId === userId;
  
  return users.find(matchId);
}





module.exports = {
    getUser,
    getUserById,
  }