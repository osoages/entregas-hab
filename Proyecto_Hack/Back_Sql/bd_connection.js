const mysql = require('mysql2/promise');

async function connection() {
    return await mysql.createConnection({
        host: 'localhost',
        user: 'coworker',
        password: '1234',
        database: 'coworking'
    });
}

module.exports = {
    connection
};