const nodemailer = require("nodemailer");

const emailUser = "test@test";


const sendMail = async (email)  => {
    console.log(emailUser)
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: "test@test", // Lista de usuarios que recibirán el mensaje
        subject: "Se ha registrado correctamente en venacow", // Subject line
        text: "Se ha registado correctamente en venacow ", // plain text body
        html: "<b>Se ha registado correctamente en venacow</b>", // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview del mensaje con el host de "pruebas" ethereal
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

sendMail().catch(console.error);

module.exports = {
    sendMail
}
