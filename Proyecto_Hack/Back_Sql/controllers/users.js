const express = require('express');
const userService = require('../service/userService');

const router = express.Router();

// Login de usuarios
router.post('/login', async function(request, response) {
    const email = request.body.email;
    const password = request.body.password;

    const responseDTO = await userService.loginUser(email, password)
    response.status(responseDTO.code).json(responseDTO);
});

// Registro de usuarios
router.post('/register', async function(request, response) {
    const phone = request.body.phone;
    const firstName = request.body.firstName;
    const lastName= request.body.lastName;
    const city = request.body.city;
    const email = request.body.email;
    const password = request.body.password;
    const role = 'user';

    if (await userService.userExist(email)) {
        const responseDTO = {'code': 200,
                            'description': 'Ya existe una cuenta asociada a (email)'};
        response.status(responseDTO.code).json(responseDTO);
        return;
    }

    const responseDTO = await userService.registerUser(phone, firstName, lastName, city, email,password,role);
    response.status(responseDTO.code).json(responseDTO);
});

router.post('/update', async function (req,res) {
    //recuperar el id del usuario


)};


module.exports = router;