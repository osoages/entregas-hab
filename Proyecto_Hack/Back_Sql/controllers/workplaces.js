const bd = require('./bd_wp');

const registerWorkPlace = async (req, res) => {
    //Se recogen los datos del body (req.body)
    const { city, name, type, prize, schedule, email, services } = req.body;
   
    //Compruebo que son válidos
    if (!city || !name || !type || !prize || !schedule || !email || !services) {
        res.status(400).send('Verifique los datos');
        return;
    }

    //Comprobación de si ya existe ese sitio dado de alta
    //En caso de que se quieran dar el Alta de puestos de trabajo dentro de un 
    //Centro de trabajo -> se debe indicar un nombre distinto
    //Ej: name: 'Escritorio 1 - Centro de trabajo Alonso' city: 'Vigo'
    if (bd.getWorkPlaceByName(name) && bd.getWorkPlaceByCity(city)) {
        res.status(409).send();
        return;
    }

    //"Pusheo el objeto en la bd"
    bd.createWorkPlace(city, name, type, prize, schedule, email, services);

    res.send();
}


module.exports = {
    registerWorkPlace
}