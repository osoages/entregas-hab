const database = require('../bd_connection');

/**
 * Función para comprobar si un usuario existe ya en la base de datos
 */

async function userExist(email) {
    const sql = 'SELECT userId FROM users WHERE email = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [email]);
    return rows.length === 1;
}

/**
 * Función que registra un usuario en la base de datos
 */


async function registerUser(phone, firstName, lastName, city, email, password, role) {
    const sql = 'INSERT INTO users (phone, firstName, lastName, city, email, password, role) VALUES (?, ?, ?, ?, ?, SHA1(?), ?)';
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [phone, firstName, lastName, city, email, password,role]);
        const responseDTO = {'code': 200,
                            'description': 'Usuario registrado correctamente',
                            'name': firstName,
                            'email': email
                        };
        return responseDTO;
    } catch (exception) {
        const responseDTO = {'code': 500,
                            'description': exception.toString()
                        };
        return responseDTO;
    }
}

async function loginUser(email, password) {
    const sql = 'SELECT userId FROM users WHERE email = ? AND password = SHA1(?)';
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [email, password]);
        let description;
        if (!rows[0]) {
            description = 'Usuario/contraseña incorrectos';
        } else {
            description = 'Usuario Logueado';
        }

        let responseDTO = {
            'code': 200,
            'description': description,
        };

        if (rows[0]) {
            responseDTO['userId'] = rows[0].userId;
        }

        return responseDTO;
    } catch (exception) {
        return {
            'code': 500,
            'description': exception.toString()
        };
    }
}


async function getUserData(userId) {
    const sql = 'SELECT * FROM users WHERE userId = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [email]);
    return rows.length === 1;
}

async function updateUser(phone, firstName, lastName, city, email, password, role) {
    const sql = 'INSERT INTO users (phone, firstName, lastName, city, email, password, role) VALUES (?, ?, ?, ?, ?, SHA1(?), ?)';
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [phone, firstName, lastName, city, email, password,role]);
        const responseDTO = {'code': 200,
                            'description': 'Datos modificados correctamente',
                            'email': email
                        };
        return responseDTO;
    } catch (exception) {
        const responseDTO = {'code': 500,
                            'description': exception.toString()
                        };
        return responseDTO;
    }
}

module.exports = {
    userExist,
    registerUser,
    loginUser
};