require('dotenv').config();


const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const { registerUser, login, infoUser, updateUser } = require('./controllers/users');
const { registerWorkPlace } = require('./controllers/workplaces');
const { isAuthenticated } = require('./middlewares/auth');

const port = process.env.PORT

const app = express();


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    console.log('Petición reiciba');
    next();
 })


//Registro de usuarios
app.post('/user/register', registerUser);
//Login de usuarios y modificación de perfil
app.post('/user/login', login);
app.get('/user/:userId', isAuthenticated, infoUser)
app.post('/user/update/:userId', isAuthenticated, updateUser)

//Endpoint de los Workplaces
app.get('/search');
//Pending to test:
app.get('/workplaces/register', registerWorkPlace);




app.listen(port, () =>{
    console.log(`Server up op PORT ${port}`)
});;