const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


//DEMO DATABASE
const bd = require('./bd_users');


const registerUser = async (req,res) => {
    //Se recogen los datos del body (req.body) que nos interesan
    const {email, password, phone, city, firstName, lastName} = req.body;

    //Comprobación de que son válidos
    if(!email || !password || !phone || !city || !firstName || !lastName){
        res.status(400).send('Revise los datos');
        return;
    }

    //Comprobación de si ya existe un usuario dado de alta
    //el sistema con ese email

    if (bd.getUser(email)) {
        res.status(409).send('Already exists an user registered with that email');
        return;
    }

    //Encriptación de la psswd
    const passwordEncripted = await bcrypt.hash(password, 12);

    //Almacenamos los datos en "la bd"
    bd.createUser(firstName, lastName, phone, city, email, passwordEncripted)

    res.send();
}


// Método para loguear al usuario
const login = async (req, res) => {
    const { email, password } = req.body;
   
    // buscar email en la bbdd
    // nos devolverá un JSON para el usuario con un ID, un role, y su password
    const user = bd.getUser(email);
           
    if (!user) {
        res.status(404).send('Does not exists an user with that email');
        return;
    }

    // error si no matchean
    const passwordIsvalid = await bcrypt.compare(password, user.password);

    if (!passwordIsvalid) {
        res.status(401).send('The password did not match with that email');
        return;
    }

    const tokenPayload = { 
        userId: user.userId, 
        role: user.role,
        email: user.email
    };

    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '1d'
    });

    res.json({
        token
    });
}

const infoUser = async (req, res) =>{

    const userId = parseInt(req.params.userId);
     
    if( userId != req.auth.userId){
        res.status(403).send('Not authorized to acces to this user profile');
        return;
    }

    const listUser = bd.getUserById(userId);
  
    res.json(listUser)
}

const updateUser = async(req, res) => {
    console.log('Intentándolo')
    //Busco el usuario que se corresponda si no casan saco un error
    const userId = parseInt(req.params.userId);

    if( userId != req.auth.userId){
        res.status(403).send(`The user with id:${userId} doesn't exist`);
        return;
    }
    console.log(userId)
    //En listUser se encuentra el usuario que se corresponde al id que quiero modificar.
    //Recupero todos los datos almacenados
    const  listUser = bd.getUserById(userId);
    
    //sustituyo cada campo del objeto almacenado en bd, por el campo que pase en req.body del post
    Object.keys(req.body).forEach(field =>{
        listUser[field] = req.body[field]
    });
    
    const {email, password, phone, city, firstName, lastName} = listUser;
    bd.patchUser(userId, firstName, lastName, phone, city, email, password);
    
    
    res.send('Los datos se han modificado correctamente');
    
    
}




module.exports = {
    registerUser,
    login,
    infoUser,
    updateUser
}