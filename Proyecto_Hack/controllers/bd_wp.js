/* Bd & Method inser for places */
/* Only business can update places */

let wpData = [
    {
        wpId: 1,
        city: 'vigo',
        name: 'demo',
        type: 'Desk',
        prize: '150',
        schedule: '09-18',
        email: 'prueba@prueba.es',
        bookingIni: '1585699200', //year:2020 month: 04 day: 01
        bookingEnd: '', //year:2020 month: 06 day: 30
        services: ['Television','Mesa','Pantalla', 'Proyector'],

        rating: [
            {
                idUser: 1,
                valoration: 5,
                message: 'Good views, bad coffe'
            }
        ]     
    }  
];

let wpId = 2;

const getWorkPlaceByName = (name) => {
    const matchName = place => place.name === name;

    return dataPlaces.find(matchName);
}

const getWorkPlaceByCity = (city) => {
    const matchCity = place => place.city === city;

    return dataPlaces.find(matchCity);
}

const createWorkPlace = (city, name, type, prize, schedule, email, services ) => {
    dataWorkPlaces.push({
        wpId: wpId++,
        city: city,
        name: name,
        type: type,
        prize: prize,
        schedule: schedule,
        email: email,
        bookingIni: [],
        bookingEnd: [],
        services: services   
    })

    console.log(wpData)
}

module.exports = {
    getWorkPlaceByName,
    getWorkPlaceByCity,
    createWorkPlace
}