/**
 * Data base & method por insert users in "bd"
 */
let users = [];
let userId = 1;

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return users.find(matchEmail);
}

//Función para comprar si ya existe un ID creado en la bd
const getUserById = (userId) => {
  
    const matchId = user => user.userId === userId;
    
    return users.find(matchId);
}

//Creación de usuarios
const createUser = ( firstName, lastName, phone, city, email, password) => {
    users.push({
        userId: userId++,
        firstName: firstName,
        lastName: lastName,
        phone: phone,
        city: city,
        email: email,
        password: password,
        role: 'user' 
    });

    //For testing the post method
    console.log(users)
}

const patchUser = (userId, firstName, lastName, phone, city, email, password) => {
    users.filter(users => users.userId === userId)
    //Only for testing
    //console.log(users)
  
  }

console.log(users)

module.exports = {
    getUser,
    createUser,
    getUserById,
    patchUser
}