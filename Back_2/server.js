/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir conciertos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 */

const axiosCacheAdapter = require('axios-cache-adapter');
const bodyParser = require('body-parser');
const express = require('express');
const axios = require('axios');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let globalId = 0;

app.use(function (req, res, next){
    console.log(`${new Date()} - Peticion recibida`)
    next();
})
//Creo la caché para almacenar los datos:
const cache = axiosCacheAdapter.setupCache({
    maxAge: 30 * 1000
  });
  
const cachedAxios = axios.create({
    adapter: cache.adapter
});

let events = [];

//Creo el endpoint
app.get('/eventos', async (req, res) => {
    res.json(events);
})

app.post('/eventos', (req, res) => {
    globalId = ++globalId;
    let eventType = req.body.type;
    const finder = events.filter(events => event.nombre === data.nombre)

    
    if (eventType === undefined){
        res.status(400).send();
        return;
    }

    //Si no pasan todos los datos error: 400, o si el tipo no es correcto
   
    let data = {
        id: globalId,
        type: req.body.type,
        nombre: req.body.nombre,
        aforo: req.body.aforo,
        artista: req.body.artista,
    }
    
    if (finder.length === 0){
        events.push(data);
    }else {
        res.status(400)
    }
   

    res.send();
});

//Puerto de escucha
app.listen(9000);
