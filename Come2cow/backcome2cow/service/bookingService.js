const database =  require ('../database');

//Función para obtener las reservas realizadas por un usuario
async function getBookedDatesByUser(userId){
    
    const sql = 
    `
    SELECT b.bookingId, b.bookingIni, b.bookingEND, b.unclean, 
    b.id_userId, b.id_wpId, b.payment, b.valorated,
    wp.name, wp.type, wp.city, wp.schedule, wp.wpId
    FROM workplaces wp
    INNER JOIN booking b ON b.id_wpId = wp.wpId
    WHERE b.id_userId = '${userId}'
    AND b.valorated = 0;	
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [userId])
    return rows;
}

async function getWpIdFromBooking(bookingId){
    const sql = `SELECT id_wpId from booking
    where bookingId = '${bookingId}' and valorated= 0`
    console.log(sql)

   const connection = await database.connection(); 

    const [rows] = await connection.execute(sql, [bookingId])
    return rows
};

async function insertReview(userId, message, stars, workplaceId){
    const sql = `
    INSERT into review
    ( valoration, message, id_userId, id_wpId )
    VALUES
    ('${stars}','${message}','${userId}','${workplaceId}')
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [userId, message, stars, workplaceId])
    return rows;
};

async function valoratedChangeStatus(bookingId) {
    const sql = `
    UPDATE booking
    SET valorated = 1
    WHERE bookingId = '${bookingId}'
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [bookingId])
    return rows;
};

async function isReserverd (bookingIni, bookingEND){
    const sql = `
    SELECT * FROM booking
    WHERE
    bookingIni >= '${bookingIni}'
    AND
    bookingEND <= '${bookingEND}'
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [bookingIni, bookingEND])
    return rows;

};

async function insertReserve(bookingIni, bookingEND, id_userId, id_wpId, payment) {
    const sql = ` INSERT INTO booking
    (bookingIni, bookingEND, unclean, id_userId, id_wpId, payment, valorated)
    VALUES
    ('${bookingIni}','${bookingEND}', 0,'${id_userId}', '${id_wpId}', '${payment}', 0)
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [bookingIni, bookingEND, id_userId, id_wpId, payment])
    return rows;
};

async function getUserEmail(id_userId){
    
    const sql = `SELECT email 
    FROM users
    WHERE userId = '${id_userId}'
    `
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [id_userId])
    console.log(rows)
    return rows;
}

module.exports = {
    getBookedDatesByUser,
    getWpIdFromBooking,
    insertReview,
    valoratedChangeStatus,
    insertReserve,
    isReserverd,
    getUserEmail
}