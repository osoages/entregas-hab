const database = require('../database');

//Función para obtener los puntos de coworking
async function getWorkplaces() {
    const sql = 
    `SELECT wp.wpId, wp.name, wp.city, wp.schedule, wp.prize, wp.type,
    AVG(rw.valoration) AS valor, 
    wpImg.image FROM workplaces wp
    INNER JOIN wpImages wpImg ON wp.wpId = wpImg.id_wpId 
    LEFT JOIN review rw ON rw.id_wpId = wp.wpId
    AND wpImg.isMiniature  = '1'
    GROUP BY wp.name    
    ` ;
    const connection = await database.connection();
    const [rows] = await connection.execute(sql);
    console.log(sql)
    return rows;
}

//Función para verificar si el espacio ya está registrado en la web con ese email
async function getWorkplaceExists(email,city){
    const sql = 'SELECT wpId FROM workplaces where email = ? AND city = ?';
        
    //Llamo a la conexión de la base de datos:
    const connection = await database.connection();
    const [rows] =  await connection.execute(sql, [email,city]);
    return rows.length === 1;
}

//Función para recuperar toda la informacíon de un workplace dado un ID
async function getWorkplacesById(id){
    const sql = 
    `SELECT workplaces.*, wpImages.image  
    FROM workplaces 
    INNER JOIN wpImages 
    ON workplaces.wpId = wpImages.id_wpId 
    WHERE wpId = ?`

    const connection = await database.connection();
    const [rowsObject] = await connection.execute(sql,[id])
    
    //Como devuelve un objeto de arrays tengo que acceder a la posición 0
    rowsWorkplace = rowsObject[0]
    
    rowsWorkplaceAsArray = [rowsWorkplace]
    //console.log(`Resultado de la query::: ${rowsWorkplace2}`)
    return rowsWorkplaceAsArray
}


//Función para recuperar todos los servicios disponibles de un workplace
async function getWorkplacesServiceById(id){
    const sql = 'SELECT name FROM services WHERE id_wpId= ?' 
    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[id]);
    console.log(rows);
    return rows
}

//Función para recuperar los workplaces filtrando por ciudad
async function getWorkplacesByCity(city){
    console.log('GetWorkplaces')

    const sql =
    `Select wp.wpId, wp.name, wp.city, wp.schedule, wp.prize, wp.type, wpImg.image,
    (select avg(valoration) from review where wp.wpId = review.id_wpId) as valor
    FROM workplaces wp
    INNER JOIN wpImages wpImg ON wp.wpId = wpImg.id_wpId 
    AND wpImg.isMiniature = '1'
    AND wp.city Like '${city}%'`


    console.log(sql)

    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[city]);

    //console.log(rows)
    return rows
}

//Función para dar de alta un espacio de coworking
async function registerWorkplace(city,name,type,prize,schedule,email){
    const sql = 'INSERT INTO workplaces (city,name,type,prize,schedule,email) VALUES (?, ?, ?, ?, ?, ?)'
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute (sql,[city,name,type,prize,schedule,email]);
        const responseDB = {
            'code':200,
            'description': `${name} se ha registrado correctamente en ${city}`,
            'name':name,
            'email': email
        }
        return responseDB;
    } catch (exception) {
       
        console.log(exception)
    }
}

//función para filtrar por parámetros de entrada (fecha)
async function getWorkplacesByFilter(city, fechaEntrada, fechaSalida){

    let sql =  `Select wp.wpId, wp.name, wp.city, wp.schedule, wp.prize, wp.type, wpImg.image,
    (select avg(valoration) from review where wp.wpId = review.id_wpId) as valor
    FROM workplaces wp
    RIGHT JOIN wpImages wpImg ON wp.wpId = wpImg.id_wpId 
    LEFT JOIN booking b ON b.id_wpId =  wp.wpId
    AND wpImg.isMiniature = '1'
    WHERE wp.city Like '${city}%'`
    let count = 0;
    let parameters = [];
    
    if (city){
        parameters.push(city)
        count ++;
    }
    console.log
    if(!isNaN (fechaEntrada && fechaEntrada) ){
        if (count > 0)
            sql += "AND";
        else
            sql += "WHERE";
        sql += ` b.bookingId NOT IN (SELECT bookingId FROM booking WHERE b.bookingIni  >= '${fechaEntrada}' AND b.bookingEND <= '${fechaSalida}')`
        parameters.push(fechaEntrada);
        parameters.push(fechaSalida);    
        count++;
    }
    sql += 'GROUP BY wp.name'
    console.log(sql)
    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[parameters]);
    return rows;
}



//Función de filtro por checkboxes
/*async function searchByInputs(arrayInput){
    //Evaluar los estados, y si cumplen añadirlos al array.
    //const arrayInput.toString();
    const sql = `
        SELECT  workplaces.* , services.* 
        FROM workplaces
        INNER JOIN services
        WHERE services.name IN (${variabledeStrings})
    `
}*/


module.exports = {
    getWorkplaces,
    getWorkplaceExists,
    registerWorkplace,
    getWorkplacesServiceById,
    getWorkplacesById,
    getWorkplacesByCity,
    getWorkplacesByFilter
}