const database = require('../database');

//Función para obtener usuarios
async function getUsers(){

    //Secuencia SQL
    const sql = 'SELECT * FROM users';

    //Conexión a la BD
    const connection = await database.connection();
    const [rows] = await connection.execute(sql);
    return {
        'code': 200,
        'data': rows
    }
}
//Función para verificar si el usuario ya está registrado en la web con el email
async function getEmailUserExists(email){
    let sql = 'SELECT userId FROM users where email = ?';
    //Llamo a la conexión de la base de datos:
    const connection = await database.connection();
    const [rows] =  await connection.execute(sql, [email]);
    return rows.length === 1;
}

async function registerUser(phone, firstName, lastName, city, email, password, role){
    //default picture
    let sql = 
    `INSERT INTO users (phone, firstName, lastName, city, email, password, role) 
    VALUES ('${phone}','${firstName}','${lastName}','${city}', 
    '${email}', SHA1('${password}'), '${role}')`

    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [phone, firstName, lastName, city, email, password, role]);
    return rows;
}

//Función para recuperar los datos de un usuario
async function getUserDataById(id){
    //Secuencia SQL
    const sql = `SELECT * FROM users  WHERE userId='${id}'`

    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[id]);

    return rows;
}

//Función de autentificación de la API

async function authUser(email, password){
    //Secuencia SQL
    const sql = `SELECT * FROM users WHERE email= ? and password = SHA1(?)`
    //establezco la conexión a la BD
    const connection = await database.connection();
    //almaceno el resultado en "rows"
    const [rows] =  await connection.execute(sql, [email, password]);
    console.log(rows);
    return rows;
}

//Función para recuperar el id del usuario desde le localStorage
async function getUserIDLocalStorage(){
    const id= localStorage.getItem('USERID')
    return id
}

//Función para actualizar los datos del usuario
async function updateUser(firstName, lastName, phone, city,email,id){
    //Secuencia SQL
    const sql = `UPDATE users
    SET firstName = ?, lastName =?, phone = ?, city = ?, email = ?
    WHERE userId = ? `
    //EStablezco la conexión a la BD
    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[firstName, lastName, phone, city, email ,id]);
    return rows
}

//Función para actualizar la contraseña de un usuario dado
async function updatePassword(password, id){
    const sql = `UPDATE users 
    SET password = SHA1(?)
    WHERE userId = ?`
    const connection = await database.connection();
    const [rows] = await connection.execute(sql,[password,id]);
    return rows
}

module.exports={
    getUsers,
    getEmailUserExists,
    registerUser,
    getUserDataById,
    authUser,
    getUserIDLocalStorage,
    updateUser,
    updatePassword
}