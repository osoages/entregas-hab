const express = require('express');
const bookingService = require('../service/bookingService');
const { response } = require('express');
const {sendMailReserve} = require('../mailsender');



const router = express.Router();

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    
    try {
        const bookedBD =  await bookingService.getBookedDatesByUser(id);
        console.log(bookedBD)
        const responseDB = {
            'code' : 200,
            'data' : bookedBD
        }
        res.status(responseDB.code).json(responseDB.data)
    }catch(error){
        console.log(error)
    }
});
router.post('/valoration/review', async (req, res) => {

    const {bookingId, message, stars, userId} = req.body;
    console.log(`Esto es el booking ID: ${bookingId}`)
    console.log(message)
    const workplaceIdToValorate = await bookingService.getWpIdFromBooking(bookingId);
    console.log(workplaceIdToValorate)
    let workplaceId =  workplaceIdToValorate[0].id_wpId
    
    try{ 
        const responseDB2Insert = await bookingService.insertReview(userId, message, stars, workplaceId)
        const changeValorated = await bookingService.valoratedChangeStatus(bookingId)
        const responseDB = {
            'code': 200,
            'data': `Valoración realizada para la reserva ${bookingId}`
        };
        res.status(responseDB.code).json(responseDB.data)
    } catch(error){
        console.log(error)
    }

});
router.post('/reserve/in', async (req, res) => {
    const {bookingIni, bookingEND, id_userId, id_wpId, payment} = req.body;
    /*if ( await bookingService.isReserverd(bookingIni,bookingEND) ){
        const responseDB = {
            'code': 222,
            'data': `El espacio ya se encuentra reservado`
        };

        res.status(responseDB.cod).json(responseDB)
        return;
    }else {*/
        
        try{
            email2Send = await bookingService.getUserEmail(id_userId);
            console.log(email2Send[0].email)

            const sendMail= sendMailReserve(email2Send[0].email, payment)
            const responseDB2Insert = await bookingService.insertReserve(bookingIni, bookingEND, id_userId, id_wpId, payment)
            const responseDB = {
                'code': 200,
                'data': `La reserva se ha generado correctamente`
            };
            res.status(responseDB.code).json(responseDB.data)
        } catch (error){
            console.log(error)
        }    
    /*}*/
     
    

});
module.exports = router;