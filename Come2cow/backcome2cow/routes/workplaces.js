const express = require('express');
const workPlacesService = require('../service/workPlacesService');
const { response } = require('express');

const router = express.Router();

//Método para listar todos los espacios de trabajo
router.get('/list', async (req, res) =>{
    //Declarar las variables que van a tomar parte en la query.
    const workplacesList = await workPlacesService.getWorkplaces();
    console.log(workplacesList)
    const responseDB = {
        'code': 200,
        'data': workplacesList
    }
    return res.status(responseDB.code).json(responseDB.data)
});

//Endpoint para recuperar todos los servicios de un workplace dado (por ID)
router.get('/services/:id', async(req, res) =>{
    //Datos que llegan desde la vista/componente
    const id = req.params.id;
    const servicesById = await workPlacesService.getWorkplacesServiceById(id);
    const responseDB = {
        'code': 200,
        'data': servicesById
    }
    
    return res.status(responseDB.code).json(responseDB.data)
})

//Endpoint para recuperar un workplace dado mediante id
router.get('/:id', async (req, res) => {
    //Datos que llegan desde la vista/componente
    const id = req.params.id;
    const workplaceById = await workPlacesService.getWorkplacesById(id);
    const responseDB = {
        'code':200,
        'data': workplaceById
    }
    console.log(responseDB.data)
    return res.status(responseDB.code).json(responseDB.data)
})

//Enpoint para filtrar los workplaces por ciudad
router.get('/list/:city', async (req, res) =>{
    //Se recoge el valor de city que llega del componente/vista
    const city = req.params.city;
    const workplacesByCity = await workPlacesService.getWorkplacesByCity(city);
    console.log(workplacesByCity)
    const responseDB = {
        'code':200,
        'data': workplacesByCity
    }
    return res.status(responseDB.code).json(responseDB.data)
})

//Endpoint para filtrar por fecha&ciudad
router.get('/booking/filter', async (req,res) => {
    const {city, fechaEntrada, fechaSalida} = req.query;
    const workplacesFiltered = await workPlacesService.getWorkplacesByFilter(city, fechaEntrada, fechaSalida);
    //console.log(workplacesFiltered);
    const responseDB = {
        'code':200,
        'data': workplacesFiltered
    }
    return res.status(responseDB.code).json(responseDB.data)
})


//ENDPOINT de registro de espacios de trabajo
router.post('/addworkplace', async (req,res) => {
    const city = req.body.city;
    const name = req.body.name;
    const type = req.body.type;
    const prize = req.body.prize;
    const schedule = req.body.schedule;
    const email = req.body.email;
 
    //Compruebo si el espacio de trabajo ya está registrado con el email en esa ciudad.
    if ( await workPlacesService.getWorkplaceExists(email,city) ){
        const responseDB = {
            'code': 404,
            'data': `Ya existe un espacio registrado con ese ${email} en ${city}`
        };
        res.status(responseDB.code).json(responseDB);
        return;
    }
    //Si el espacio de trabajo no está registrado, ejecuto la función de registro pasando los datos facilitados por el input
    const responseDB = await workPlacesService.registerWorkplace(city,name,type,prize,schedule,email);

    res.status(responseDB.code).json(responseDB);
});



module.exports = router;