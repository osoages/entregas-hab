require('dotenv').config();
const {sendMailRegister} = require('../mailsender');


//Librerías requeridas
const express = require('express');
const userService = require('../service/userService');
const jwt = require ('jsonwebtoken');
const router = express.Router();

const key = process.env.SECRET;

router.get('/', async (req,res) =>{
    //Declarar las variables que van a tomar parte en la query.
    try{
        const responseDB = await userService.getUsers();
        res.status(responseDB.code).json(responseDB)
    }
    catch(error){
        console.log(error)
    }
});


//ENDPOINT de registro de usuario
router.post('/register', async (req,res) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const city = req.body.city;
    const email = req.body.email;
    const password = req.body.password;
    const role = 'user';


    //Compruebo si el usuario ya está registrado con el email que pasa.
    if ( await userService.getEmailUserExists(email) ){

        const responseDB = {
            'code': 301,
            'data': `El usuario ${email} ya está registrado`
        };
        res.status(responseDB.code).json(responseDB);
        
        return ;
    } else {
        try{
            console.log('Intenando el insert')
            console.log(email)
            const insertDB = await userService.registerUser(phone, firstName, lastName, city, email, password, role);
            
            const sendmail= sendMailRegister(email);
            const responseDB = {
                'code': 200,
                'data': `Usuario creado correctamente`
            }
            res.status(responseDB.code).json(responseDB)
            
        } catch(error){
            const responseDB = {
                'code': 304,
                'data': `Datos erroneos`
            }
        }
    }
});


//Endpoint para recuperar la información del usuario y que acceda a sus datos
router.get('/info/:id', async(req, res) =>{
    //ID del usuario
    id = req.params.id
    //console.log('Se ejecuta la query del login Estoy en users.js')
    try{
        const infoUser = await userService.getUserDataById(id);
        const responseDB = {
            'code': 200,
            'data': infoUser
        }
        console.log(infoUser)
        res.status(responseDB.code).json(responseDB.data)

    }
    catch(error){
        console.log(error)
    }
});

router.post('/auth', async (req, res) => {
    //Datos que se reciben desde el front
    const email = req.body.email;
    const password = req.body.password;
    const responseDB = await userService.authUser(email,password)
    if (responseDB.length >0){
        const payload = {
            check: true
        }
        let admin = null;
        if (responseDB[0].role === 'admin'){
            admin = true;
        } else {
            admin = false;
        }

        //Guardo el email del usuario
        let email = '';
        email = responseDB[0].email;
        //Guardo el id del usuario
        let userId= '';
        userId = responseDB[0].userId;

        //Asigno el token a una variable
        //TOKEN
        const token = jwt.sign(payload, key,{
            expiresIn: '2 days'
        })
        res.json({
            code:'200',
            mensaje: 'Ingreso realizado con éxito',
            token: token,
            admin: admin,
            email: email,
            userId: userId
        })
    } else {
        const responseERR=({
            code:'301',
            error:'Datos incorrectos'
        });
        res.status(responseERR.code).json(responseERR)
        console.log('Datos incorrectos')
    }

    
    //Compruebo si existe un usuario con esos datos
})


router.put('/update/:id', async (req,res) => {
    const id = req.params.id;

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const city = req.body.city;
    const email = req.body.email;

    try{
        const responseUpdate = await userService.updateUser(firstName, lastName, phone, city, email, id);
        const responseDB = {
            'code': 200,
            'message': 'Usuario actualizado Maravillosamente'
        }
        res.status(responseDB.code).json(responseDB)

    } catch (error){
        console.log(error)
    }
})

router.put('/updatepw/:id', async (req, res) => {
    const id = req.params.id;

    const password = req.body.password;
    try{
        const responseUpdatePw = await userService.updatePassword(password, id);
        const responseDB = {
            'code': 200,
            'message':'Contraseña actualizada'
        }
        res.status(responseDB.code).json(responseDB)
    }catch (error){
        console.log(error)
    }
})

module.exports = router;