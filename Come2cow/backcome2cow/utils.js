import jwt from 'jwt-decode'
import axios from 'axios'


const ENDPOINT = 'http://localhost:8090'

//Función de login que se llamará desde la vista de Login

export function login(email, password){
        console.log('antes del axios return')
        return axios.post(`${ENDPOINT}/users/auth`, {
            email: email,
            password: password,
        })
        
        .then (function(response){
            if (response.data.code !== '200'){ç
                return
            }
            else if (response.data.code === '200'){
                //Me guardo el TOKEN
                setAuthToken(response.data.token)
                
                //Me guardo el ROL
                setIsAdmin(response.data.admin)

                //Me guardo el email de usuario
                setUserEmail(response.data.email)

                //Me guardo el ID del usuario
                setUserID (response.data.userId)
            }
            
        })
        .catch(function(response){ 
            console.log(error)
    });
}

//Guardar el token recibido del login en localStorage
export function setAuthToken (token){
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    localStorage.setItem('AUTH_TOKEN_KEY', token);
}

//Función para recuperar el token desde el localstorage
export function getAuthToken(){
    return localStorage.getItem('AUTH_TOKEN_KEY')
}

//Función para conseguir la fecha de caducidad del Token
export function getTokenExpiration(encodedToken){
    let token = jwt(encodedToken)
    if(!token.exp){
        return null
    }
    let date = new Date(0)
    date.setUTCSeconds(token.exp)
    return date
}

// Función que comprueba si el token está caducado o no
export function isExpired (token){
    let expirationDate = getTokenExpiration(token)
    return expirationDate < new Date()
}

//Función que comprueba si la persona está logueada y su token es válido
export function isLoggedIn(){
    let authToken = getAuthToken()
    //authToken comprueba si está o no logueado, y isExpired verifica si el authToken está o no expirado
    return !!authToken && !isExpired(authToken) //!! <- devuelve un booleano
}

//Función para guardar ADMIN en localStorage

export function setIsAdmin(admin) {
    localStorage.setItem('ROLE', admin)
}

//Función para recuperar el valor de la 'variable' ADMIN del localstorage 
export function getIsAdmin(){
    return localStorage.getItem('ROLE')
}

//Función para saber si es admin o no
export function checkIsAdmin(){
    let role = null
    let admin = getIsAdmin()
    
    //Todo lo que se guarda en localStorage se guarda en formato STRING
    if (admin === 'true'){
        role = true
    } else {
        role = false
    }
    return role
}

//Función para almacenar el email del usuario en localStorage
export function setUserEmail (email){
    return localStorage.setItem('EMAIL', email)
}

//Funciones para guardar el userID y recuperarlo desde el localStorage
export function setUserID (userId){
    return localStorage.setItem('USERID', userId)
}


//Función para recuperar el email de usuario desde el localStorage
export function getUserEmailLocalStorage(){
    return localStorage.getItem('EMAIL')
}

//Función para recuperar el userId de usuario desde el localStorage
export function getUserUserIdLocalStorage(){
    return localStorage.getItem('USERID')
}

//Función de LOGOUT
export function logout(){
    axios.defaults.headers.common['Authorization'] = '';
    localStorage.removeItem('AUTH_TOKEN_KEY')
    localStorage.removeItem('ROLE')
    localStorage.removeItem('EMAIL')
    localStorage.removeItem('USERID')
}