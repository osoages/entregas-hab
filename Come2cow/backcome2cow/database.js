//Modulo de conexión a la BD
const mysql = require('mysql2/promise');

//Conexión a la BD
async function connection(){
    return await mysql.createConnection({
        host: 'localhost',
        user: 'coworker',
        password: '1234',
        database: 'coworking'
    });
};

module.exports={
    connection
};