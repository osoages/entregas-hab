const nodemailer = require('nodemailer')

async function sendMailRegister(email) {
  let transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: 'testingosoages@gmail.com',
      pass: 'hackabosstesting'
    }
  });
  
  let mailOptions = {
    from: 'testingosoages@gmail.com',
    to: `${email}`,
    subject: 'Regisro en COME2COW',
    html: `<h1>Bienvenido a COME2COW</h1>
            Tu cuenta con el correo: ${email} ya está activada
    `
  };
  
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log(`Email sent: ${info.response}`);
    }
  });
}

async function sendMailReserve(email,  payment) {
  let transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: 'testingosoages@gmail.com',
      pass: 'hackabosstesting'
    }
  });
  
  let mailOptions = {
    from: 'testingosoages@gmail.com',
    to: `${email}`,
    subject: 'Registro de Espacio Coworking',
    html: `<h1>Reserva Realizada</h1>
            <body>Ha reservado correctamente por un total de ${payment}</body>
    `
  };
  
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log(`Email sent: ${info.response}`);
    }
  });
}

  module.exports = {
    sendMailRegister,
    sendMailReserve
  }