require('dotenv').config();
//Librerías instaladas
const cors = require('cors');
const axios = require('axios');
const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/users');
const bookingRoutes = require('./routes/booking')
const workplacesRoutes = require('./routes/workplaces');

const port = process.env.PORT;

const app = express();

//Librerías empleados por la APP
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json ());

//Importo los endpoints definidos en users
app.use('/users', userRoutes);
app.use('/workplaces', workplacesRoutes);
app.use('/booking/', bookingRoutes);


app.listen(port, () => console.log(`Api alive en ${port}`))