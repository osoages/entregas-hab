import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vueHeadful from 'vue-headful'
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);

Vue.component('vue-headful', vueHeadful)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
