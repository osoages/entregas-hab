import Vue from 'vue'
import VueRouter from 'vue-router'
import LandingSearch from '../views/LandingSearch.vue'
import Error from '../views/Error.vue'

import {isLoggedIn, checkIsAdmin} from '../../../backcome2cow/utils.js'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'LandingSearch',
    component: LandingSearch,
    meta:{
      allowAnon:true //Anons pueden acceder
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta:{
      allowAnon:false //Anons NO pueden acceder
    }
  },
  {
    path: '/register',
    name: 'Register',
    component:
     () => import('../views/Register.vue'),
     meta:{
      allowAnon:true //Anons pueden acceder
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta:{
      allowAnon:true //Anons pueden acceder
    }
  },
  {
    path: '/workplaces',
    name: 'Workplaces',
    component: () => import('../views/Workplaces.vue'),
    meta:{
      allowAnon:true //Anons  pueden acceder
    }
  },
  {
    path: '/mycoworking',
    name: 'Mycoworking',
    component: () => import('../views/Mycoworking.vue'),
    meta:{
      allowAnon:false //Anons NO pueden acceder
    }
  },
  {
    path: '/workplacesinfo/:id',
    name: 'WorkplacesInfo',
    component: () => import('../views/Workplaceinfo.vue'),
    meta:{
      allowAnon: false //Anons NO pueden acceder
    }
  },
  {
    path: '/workplaces/add',
    name: 'Newworkplace',
    component: () => import('../views/Newworkplace'),
    meta:{
      allowAnon:false //Anons NO pueden acceder
    }
  },
  {
    path: '*',
    name: 'Error',
    component: Error,
    meta: {
      allowAnon: true //Perrmite acceder a los usuarios anónimos
    }
  }
  
]

const router = new VueRouter({
  routes
})

//Verifico si los usuarios que acceden al sitio están logueados
router.beforeEach( (to, from, next) => {
  if( !to.meta.allowAnon && !isLoggedIn() ){
    next( {
      path:'/home',
      query: { redirect: to.fullPath }
    } )
  } else{
    next()
  }
})


export default router
